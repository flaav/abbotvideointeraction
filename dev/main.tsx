import * as React from "react";
import * as ReactDOM from "react-dom";
import { VideoPlayer } from './components/videoplayer';
const {Component} = React;


ReactDOM.render(<VideoPlayer side="left" />, document.getElementById("app"));