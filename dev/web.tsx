import * as React from "react";
import * as ReactDOM from 'react-dom';

const { Component } = React;

declare var io : any;
let socket = io.connect(`http://${location.host}`);
interface IApp {
    page : number;
    timer : number;
    timerOn : boolean;
}
class App extends Component<{},IApp> {
    private timer : any;
    constructor(props){
        super(props);
        this.state = {
            page : 0,
            timer : 0,
            timerOn : false
        };
        this.timer = null;
    }
    componentDidMount() {
        console.log("mtds");
        socket.on("wait", time =>{
            console.log("wait", time , "seconds");
            this.initTimer(time.toFixed(0));
        });
    }
    initTimer = (ev : number) => {
        if(this.timer) {
            clearInterval(this.timer);
            this.timer = null;
        }
        this.setState({
            timerOn : true,
            timer : ev
        });
        this.timer = setInterval(ev => {
            let last = this.state.timer;
            this.setState({
                timer : --last
            });
            if(last <= 0){
                clearInterval(this.timer);
                this.timer = null;
                this.setState({
                    timer : 0,
                    timerOn : false
                });
            }
        }, 1000);
    }
    setPage = (nid : number, side : string) => {
        console.log("vid", nid , side);
        let id = nid + 1;
        socket.emit("play", { id,side});
    }
    render() {
        const wrapper : any = {
            marginTop : "130px",
            width : "1024px",
            height : "768px",
            position : "absolute",
            top : 0,
            left : 0,
            background : "-webkit-linear-gradient(left top,white ,blue 90%, blue)"
        }
        const tabs : any = {
            width : "1024px",
            height : "100px",
            position : "absolute",
            bottom : 0,
            left: 0,
            background : "transparent"
        }
        const button : any = {
            width : "80px",
            height : "80px",
            borderRadius : "40px",
            border : "none",
            background : this.state.page === 0 ? "white" : "rgba(0,0,255,.6)",
            opacity : this.state.page === 0 ? "1" : ".5",
            textDecoration: this.state.page === 0 ? "underline" : "none",
            margin : "10px 10px",
            transition : "all .5s linear",
            fontSize : "20px",
            WebkitFilter : "drop-shadow(2px 2px 4px rgba(0,0,0,.2))"
        }
        const button2 : any = {
            width : "80px",
            height : "80px",
            borderRadius : "40px",
            border : "none",
            background : this.state.page === 1 ? "white" : "rgba(0,0,255,.6)",
            textDecoration: this.state.page === 1 ? "underline" : "none",
            margin : "10px 10px",
            opacity : this.state.page === 1 ? "1" : ".5",
            transition : "all .5s linear",
            fontSize : "20px",
            WebkitFilter : "drop-shadow(2px 2px 4px rgba(0,0,0,.2))"
        }
        const button3 : any = {
            width : "80px",
            height : "80px",
            borderRadius : "40px",
            border : "none",
            background : this.state.page === 2 ? "white" : "rgba(0,0,255,.6)",
            textDecoration: this.state.page === 2 ? "underline" : "none",
            opacity : this.state.page === 2 ? "1" : ".5",
            margin : "10px 10px",
            transition : "all .5s linear",
            fontSize : "20px",
            WebkitFilter : "drop-shadow(2px 2px 4px rgba(0,0,0,.2))"
        }
        const timer : any = {
            position : "absolute",
            width : "50px",
            height : "50px",
            top : "calc(50% - 25px)",
            left : "calc(50% - 25px)",
            pinterEvents : "none",
            opacity : this.state.timerOn? "1" : "0"
        }
        const desc : any = {
            color : "white",
            position : "absolute",
            right : "50px",
            top : "50px",
            fontSize : "22px"
        }
        return (<div style={wrapper}> 
            <div style={desc}> Tocca sul pulsante per sentire la risposta ...   </div>
            <Page setP={this.setPage} current={this.state.page} id={0}/>
            <Page setP={this.setPage} current={this.state.page} id={1}/>
            <Page setP={this.setPage} current={this.state.page} id={2}/>
            <div style={tabs}>
                <span style={{textAlign : "center", paddingLeft : "350px"}}>
                    <button style={button} onTouchEnd={e => this.setState({page : 0})}> 1 </button>
                    <button style={button2} onTouchEnd={e => this.setState({page : 1})}> 2 </button>
                    <button style={button3} onTouchEnd={e => this.setState({page : 2})}> 3 </button>
                </span>
            </div>
            <div style={timer}> {this.state.timer} </div>
        </div>);
    }
}
interface IPage {
    current : number;
    id : number;
    setP?(id : number, side : string): void
}
class Page extends Component<IPage,{}> {
    private text : Array<string>;
    private leftChart : SVGCircleElement;
    private rightChart : SVGCircleElement;
    constructor(props){
        super(props);
        this.text = [
            "Quali sono le sfide principali che lei deve affrontare nel breve e nel lungo termine?",
            "Cosa potrebbe aiutarla nell'affrontare queste sfide?",
            "Cosa ha visto recentemente?"
        ];
    }
    componentDidMount() {
        socket.on("update", data => {
            if(this.props.id + 1  === data.id ){
                let nr = 500 - (Number(data.time) * 5);
                
                if(nr === 0){
                    this.leftChart.style.strokeDashoffset = `500`;
                    this.rightChart.style.strokeDashoffset = `500`;
                    return;
                }
                if(data.side === "left"){
                    this.leftChart.style.strokeDashoffset = `${nr}`;
                }else{
                   this.rightChart.style.strokeDashoffset = `${nr}`;
                }
            }else{
                this.leftChart.style.strokeDashoffset = `500`;
                this.rightChart.style.strokeDashoffset = `500`;
            }
        });
    }
    componentWillReceiveProps(np){
      //  console.log("new", np);
    }
    render() { 
        const wrapper : any = {
            width : "100%",
            height : "calc(100% - 100px)",
            position : "absolute",
            top : 0,
            left : 0,
            background : "transparent",
            transition : "all .6s ease-out",
            WebkitTransform  : this.props.current === this.props.id? "scale(1)" : "scale(0)",
            opacity : this.props.current === this.props.id? "1" : "0",
            pointerEvents :  this.props.current === this.props.id? "auto" : "none"
        }
        const title : any = {
            position : "absolute",
            boxSizing : "border-box",
            background : "white",
            padding : "20px 50px",
            width : "100%",
            top : "100px",
            boxShadow : "0px 5px 10px -5px black",
            textAlign : "center"
        }

        const leftPanel : any = {
            position : "absolute",
            width : "300px",
            height : "300px",
            background : "transparent",
            left : "100px",
            bottom : "100px"
        }
        const rightPanel : any = {
            position : "absolute",
            width : "300px",
            height : "300px",
            background : "transparent",
            right : "100px",
            bottom : "100px"
        }
        const label : any = {
            position : "absolute",
            width : "100%",
            background : "rgba(0,0,255,0)",
            color : "white",
            bottom : 0,
            left : 0,
            margin : 0,
            textAlign : "center"
        }
        const btn : any = {
            position : "absolute",
            width : "150px",
            height : "150px",
            top : "75px",
            left : "75px",
            border : "none",
            background : "transparent",
            borderRadius : "75px"
        }
        const svg : any = {
            position : "absolute",
            width : "150px",
            height : "150px",
            top : "75px",
            left: "75px",
            background: "white",
            WebkitTransform : "rotate(-90deg)",
            borderRadius : "75px",
            pointerEvents : "none",
            WebkitFilter : "drop-shadow(-5px 2px 5px rgba(0,0,0,.5))"
        }
        const circle : any = {
            fill : "white",
            stroke : "blue",
            strokeWidth : "150",
            transition : "stroke-dashoffset .5s linear",
            strokeDasharray: "500 500",
            strokeDashoffset : "500"
        }
        return (<div style={wrapper}> 
            <h1 style={title}> {this.text[this.props.id]} </h1>

            <div style={leftPanel}>
                <svg style={svg}> 
                    <circle id="circleLeft" ref={c => this.leftChart = c} style={circle} r="75px" cx="75px" cy="75px" />
                </svg>  
                <button style={btn} onTouchStart={() => this.props.setP(this.props.id, "left")}> LEFT </button>  
                <h2 style={label}> Lab Director </h2>
            </div>
            <div style={rightPanel}>
           
            <svg style={svg}> 
                    <circle id="circleRight" ref={c => this.rightChart = c} style={circle} r="75" cx="75" cy="75" />
            </svg> 
            <button style={btn} onTouchStart={() => this.props.setP(this.props.id, "right")}> RIGHT </button>
            <h2 style={label}> Hospital Manager </h2>
            </div>
            


           

        </div>);
    }
}

ReactDOM.render(<App />, document.getElementById('app'));
