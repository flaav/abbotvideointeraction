declare var io : any;

interface ISocket {
    emit(type : string, data : any);
    on(action : string, data : any);
}
console.log(location.host);
var host = location.host;
var socket : ISocket = io.connect(`http://${host ? host : "localhost:3000"}`);


export default socket;
