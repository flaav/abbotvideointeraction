import * as React from "react";
const { Component } = React;
import socket from "./messenger";

Object.defineProperty(HTMLMediaElement.prototype, 'playing', {
    get: function(){
        return !!(this.currentTime > 0 && !this.paused && !this.ended && this.readyState > 2);
    }
});


interface IVideoPlayer {
    side : string;
}

export class VideoPlayer extends Component<IVideoPlayer,{}>{
    private videos : Array<string>;
    private videoNode : HTMLVideoElement;
    private videoSby : HTMLVideoElement;
    private videoId : number;
    private tail : boolean;
    constructor(props){
        super(props);

        this.videos = [
            `${__dirname}/videos/${this.props.side}/standby.mp4`,
            `${__dirname}/videos/${this.props.side}/video1.mp4`,
            `${__dirname}/videos/${this.props.side}/video2.mp4`,
            `${__dirname}/videos/${this.props.side}/video3.mp4`,
        ];
       this.videoId = 0;
       this.tail = false;
    }
    componentDidMount() {
        socket.on("play", data => {
            
            if(this.props.side === data.side){
                if(this.videoId === data.id){
                    console.log("we playing alredy...");
                    return;
                }
                this.videoNode.src = this.videos[data.id];
                this.videoId = data.id;
                this.videoNode.style.opacity = "0";
                this.videoNode.currentTime = 0;
                this.videoSby.play();
                let time = this.videoSby.duration - this.videoSby.currentTime;
                socket.emit("wait", time);
                this.tail = true;
            } else {
                this.tail = false;
                this.videoNode.pause();
                this.videoNode.style.opacity = "0";
                this.videoSby.play();
                this.videoId = 0;
            }

        });
    }
    play = ev => {
        console.log("play");
    }
    update = ev => {

        let side = this.props.side;
        let id = this.videoId;
        let time = ((ev.target.currentTime / ev.target.duration) * 100).toFixed(0);

        console.log("wtfttt",time);


    socket.emit("update", {side ,id ,time});
    }
    ended = ev => {
        console.log("ended");
        this.videoNode.style.opacity = "0";
        this.videoSby.currentTime = 0;
        this.videoSby.play();
        this.tail = false;
        this.videoId = 0;
    }
    standByEnded = ev => {
        console.log("suka");
        if(!this.tail) {
            ev.target.play();
        }else{
            this.videoNode.play();
           // this.videoSby.pause();
            this.videoNode.style.opacity = "1";
        }
    }
    render() {
        const wrapper : any = {
            width : "100%",
            height: "100%",
            position : "absolute",
            left : "0",
            top : "0",
            background : "blue"
        }
        const video : any = {
            height : "100%",
            position : "absolute"
        }
        const vidActive : any = {
            height : "100%",
            position : "absolute",
            opacity : "0",
            transition : "all .2s ease-in-out"
        }
        return(<div style={wrapper}>
            <video onEnded={this.standByEnded} ref={v => this.videoSby = v} style={video} autoPlay src={this.videos[0]}>
                
            </video>
            <video onEnded={this.ended} onTimeUpdate={this.update} onPlay={this.play} ref={v => this.videoNode = v} style={vidActive} src=""> </video>
        </div>);
    };
}