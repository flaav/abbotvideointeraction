interface ICoord {
    x : number;
    y : number;
}
export default class Person {
    private element : HTMLDivElement;
    private x : number;
    private y : number;
    private infected : boolean;
    private immunity : number;
    private limit : ICoord;
    private vaccined : boolean;
    constructor(el : HTMLDivElement, x : number, y : number, infected : boolean, immunity : number) {
        this.element = el;
        this.x = x;
        this.y = y;
        this.infected = infected;
        this.immunity = immunity;
        this.limit = { x : 128, y : 72};
        this.vaccined = false;
    }
    vaccinate() {
        this.vaccined = true;
        this.element.style.background = "red";
    }
    spread = (items : any) => {
        console.log("infecting");
        if(this.infected) {
            return;
        }
        this.infected = true;
        if(this.vaccined && Math.random() < 0.5){
            return;
        }

        if(Math.random() > 0.5){
            return;
        }

        let vicini : any = new Array();
            // top left
        if(this.x > 0 && this.y > 0) {
            vicini.push(items[this.x - 1][this.y - 1]);
        }
            // left
        if(this.x > 0) {
            vicini.push(items[this.x - 1][this.y]);
        }
            // bot left
        if(this.x > 0 && this.y < this.limit.y - 1) {
            vicini.push(items[this.x - 1][this.y + 1]);
        }
            // top
        if(this.y > 0) {
            vicini.push(items[this.x][this.y - 1]);
        }
            // top right
        if(this.x < this.limit.x - 1 && this.y > 0) {
            vicini.push(items[this.x + 1][this.y - 1]);
        }
            // right
        if(this.x < this.limit.x - 1) {
            vicini.push(items[this.x + 1][this.y]);
        }

            // bot
        if(this.y < this.limit.y - 1) {
            vicini.push(items[this.x][this.y + 1]);
        }

            // bot right
        if(this.x < this.limit.x -1 && this.y < this.limit.y - 1) {
            vicini.push(items[this.x + 1][this.y +1]);
        }
       // this.element.classList.add("infected");
        this.element.style.transition = `all 50ms linear`;
        this.element.style.background = "blue";
        setTimeout(() => {
            vicini.map( e => e.spread(items));
        }, 100);
               
        
    }
}