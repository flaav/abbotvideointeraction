const electron = require("electron");
const { app, BrowserWindow } = electron;
const path = require('path');

var express = require('express');
var socket = require('socket.io');
var WebApp = express();

WebApp.use(express.static(__dirname + "/public"));

var server = WebApp.listen(3000);


var io = socket(server);
console.log("FUCK FUCK FUCK");
io.on("connection", socket => {
    socket.on("play", data => {
        io.sockets.emit("play", data);
    });
    socket.on("update", data => {
        io.sockets.emit("update", data);
    });
    socket.on("wait", time => {
        io.sockets.emit("wait", time);
    });
});



const NODE_ENV = "dev";

let firstWindow, secondWindow;


function createFirstWindow() {
    firstWindow = new BrowserWindow({ kiosk: true });
    firstWindow.setMenu(null);
    firstWindow.loadURL(path.resolve(__dirname, "index1.html"));
    if (NODE_ENV === "dev") firstWindow.openDevTools();
    firstWindow.on('closed', () => {
        firstWindow = null;
        secondWindow = null;
        app.quit();
    });
}

function createSecondWindow(ext) {
    secondWindow = new BrowserWindow({
        x: ext.bounds.x + 50,
        y: ext.bounds.y + 50,
        kiosk: true
    });
    secondWindow.setMenu(null);
    if (NODE_ENV === "dev") secondWindow.openDevTools();
    secondWindow.on('closed', () => secondWindow = null);
    secondWindow.loadURL(path.resolve(__dirname, "index2.html"));
}

app.on('ready', ev => {

    let displays = electron.screen.getAllDisplays();

    let ext = displays.find(d => d.bounds.x !== 0 || d.bounds.y !== 0);

    if (ext) {
        createSecondWindow(ext);
        createFirstWindow();
    } else { createFirstWindow(); }

});