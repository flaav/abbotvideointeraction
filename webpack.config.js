const path = require('path');

module.exports = {
    entry: { "./app/main": "./dev/main.tsx", "./app/main2": "./dev/main2.tsx", "./public/app": "./dev/web.tsx" },
    output: {
        filename: "[name].bundle.js",
        path: path.resolve(__dirname)
    },
    devtool: "inline-source-map",
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".json", ".css"]
    },
    module: {
        rules: [
            { test: /\.tsx?$/, loader: "awesome-typescript-loader" },
            { test: /\.css$/, loader: "style-loader!css-loader?modules&importLoaders=1&localIdentName=[local]_[hash:base64:4]" }
        ]
    },
    externals: {
        "react": "React",
        "react-dom": "ReactDOM"
    },
    // target: "electron"
    target: 'node',
    node: {
        __dirname: false,
        __filename: false,
    }
};