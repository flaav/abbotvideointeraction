/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 6);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = React;

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = ReactDOM;

/***/ }),
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = __webpack_require__(0);
var ReactDOM = __webpack_require__(1);
var Component = React.Component;
var socket = io.connect("http://" + location.host);
var App = /** @class */ (function (_super) {
    __extends(App, _super);
    function App(props) {
        var _this = _super.call(this, props) || this;
        _this.initTimer = function (ev) {
            if (_this.timer) {
                clearInterval(_this.timer);
                _this.timer = null;
            }
            _this.setState({
                timerOn: true,
                timer: ev
            });
            _this.timer = setInterval(function (ev) {
                var last = _this.state.timer;
                _this.setState({
                    timer: --last
                });
                if (last <= 0) {
                    clearInterval(_this.timer);
                    _this.timer = null;
                    _this.setState({
                        timer: 0,
                        timerOn: false
                    });
                }
            }, 1000);
        };
        _this.setPage = function (nid, side) {
            console.log("vid", nid, side);
            var id = nid + 1;
            socket.emit("play", { id: id, side: side });
        };
        _this.state = {
            page: 0,
            timer: 0,
            timerOn: false
        };
        _this.timer = null;
        return _this;
    }
    App.prototype.componentDidMount = function () {
        var _this = this;
        console.log("mtds");
        socket.on("wait", function (time) {
            console.log("wait", time, "seconds");
            _this.initTimer(time.toFixed(0));
        });
    };
    App.prototype.render = function () {
        var _this = this;
        var wrapper = {
            marginTop: "130px",
            width: "1024px",
            height: "768px",
            position: "absolute",
            top: 0,
            left: 0,
            background: "-webkit-linear-gradient(left top,white ,blue 90%, blue)"
        };
        var tabs = {
            width: "1024px",
            height: "100px",
            position: "absolute",
            bottom: 0,
            left: 0,
            background: "transparent"
        };
        var button = {
            width: "80px",
            height: "80px",
            borderRadius: "40px",
            border: "none",
            background: this.state.page === 0 ? "white" : "rgba(0,0,255,.6)",
            opacity: this.state.page === 0 ? "1" : ".5",
            textDecoration: this.state.page === 0 ? "underline" : "none",
            margin: "10px 10px",
            transition: "all .5s linear",
            fontSize: "20px",
            WebkitFilter: "drop-shadow(2px 2px 4px rgba(0,0,0,.2))"
        };
        var button2 = {
            width: "80px",
            height: "80px",
            borderRadius: "40px",
            border: "none",
            background: this.state.page === 1 ? "white" : "rgba(0,0,255,.6)",
            textDecoration: this.state.page === 1 ? "underline" : "none",
            margin: "10px 10px",
            opacity: this.state.page === 1 ? "1" : ".5",
            transition: "all .5s linear",
            fontSize: "20px",
            WebkitFilter: "drop-shadow(2px 2px 4px rgba(0,0,0,.2))"
        };
        var button3 = {
            width: "80px",
            height: "80px",
            borderRadius: "40px",
            border: "none",
            background: this.state.page === 2 ? "white" : "rgba(0,0,255,.6)",
            textDecoration: this.state.page === 2 ? "underline" : "none",
            opacity: this.state.page === 2 ? "1" : ".5",
            margin: "10px 10px",
            transition: "all .5s linear",
            fontSize: "20px",
            WebkitFilter: "drop-shadow(2px 2px 4px rgba(0,0,0,.2))"
        };
        var timer = {
            position: "absolute",
            width: "50px",
            height: "50px",
            top: "calc(50% - 25px)",
            left: "calc(50% - 25px)",
            pinterEvents: "none",
            opacity: this.state.timerOn ? "1" : "0"
        };
        var desc = {
            color: "white",
            position: "absolute",
            right: "50px",
            top: "50px",
            fontSize: "22px"
        };
        return (React.createElement("div", { style: wrapper },
            React.createElement("div", { style: desc }, " Tocca sul pulsante per sentire la risposta ...   "),
            React.createElement(Page, { setP: this.setPage, current: this.state.page, id: 0 }),
            React.createElement(Page, { setP: this.setPage, current: this.state.page, id: 1 }),
            React.createElement(Page, { setP: this.setPage, current: this.state.page, id: 2 }),
            React.createElement("div", { style: tabs },
                React.createElement("span", { style: { textAlign: "center", paddingLeft: "350px" } },
                    React.createElement("button", { style: button, onTouchEnd: function (e) { return _this.setState({ page: 0 }); } }, " 1 "),
                    React.createElement("button", { style: button2, onTouchEnd: function (e) { return _this.setState({ page: 1 }); } }, " 2 "),
                    React.createElement("button", { style: button3, onTouchEnd: function (e) { return _this.setState({ page: 2 }); } }, " 3 "))),
            React.createElement("div", { style: timer },
                " ",
                this.state.timer,
                " ")));
    };
    return App;
}(Component));
var Page = /** @class */ (function (_super) {
    __extends(Page, _super);
    function Page(props) {
        var _this = _super.call(this, props) || this;
        _this.text = [
            "Quali sono le sfide principali che lei deve affrontare nel breve e nel lungo termine?",
            "Cosa potrebbe aiutarla nell'affrontare queste sfide?",
            "Cosa ha visto recentemente?"
        ];
        return _this;
    }
    Page.prototype.componentDidMount = function () {
        var _this = this;
        socket.on("update", function (data) {
            if (_this.props.id + 1 === data.id) {
                var nr = 500 - (Number(data.time) * 5);
                if (nr === 0) {
                    _this.leftChart.style.strokeDashoffset = "500";
                    _this.rightChart.style.strokeDashoffset = "500";
                    return;
                }
                if (data.side === "left") {
                    _this.leftChart.style.strokeDashoffset = "" + nr;
                }
                else {
                    _this.rightChart.style.strokeDashoffset = "" + nr;
                }
            }
            else {
                _this.leftChart.style.strokeDashoffset = "500";
                _this.rightChart.style.strokeDashoffset = "500";
            }
        });
    };
    Page.prototype.componentWillReceiveProps = function (np) {
        //  console.log("new", np);
    };
    Page.prototype.render = function () {
        var _this = this;
        var wrapper = {
            width: "100%",
            height: "calc(100% - 100px)",
            position: "absolute",
            top: 0,
            left: 0,
            background: "transparent",
            transition: "all .6s ease-out",
            WebkitTransform: this.props.current === this.props.id ? "scale(1)" : "scale(0)",
            opacity: this.props.current === this.props.id ? "1" : "0",
            pointerEvents: this.props.current === this.props.id ? "auto" : "none"
        };
        var title = {
            position: "absolute",
            boxSizing: "border-box",
            background: "white",
            padding: "20px 50px",
            width: "100%",
            top: "100px",
            boxShadow: "0px 5px 10px -5px black",
            textAlign: "center"
        };
        var leftPanel = {
            position: "absolute",
            width: "300px",
            height: "300px",
            background: "transparent",
            left: "100px",
            bottom: "100px"
        };
        var rightPanel = {
            position: "absolute",
            width: "300px",
            height: "300px",
            background: "transparent",
            right: "100px",
            bottom: "100px"
        };
        var label = {
            position: "absolute",
            width: "100%",
            background: "rgba(0,0,255,0)",
            color: "white",
            bottom: 0,
            left: 0,
            margin: 0,
            textAlign: "center"
        };
        var btn = {
            position: "absolute",
            width: "150px",
            height: "150px",
            top: "75px",
            left: "75px",
            border: "none",
            background: "transparent",
            borderRadius: "75px"
        };
        var svg = {
            position: "absolute",
            width: "150px",
            height: "150px",
            top: "75px",
            left: "75px",
            background: "white",
            WebkitTransform: "rotate(-90deg)",
            borderRadius: "75px",
            pointerEvents: "none",
            WebkitFilter: "drop-shadow(-5px 2px 5px rgba(0,0,0,.5))"
        };
        var circle = {
            fill: "white",
            stroke: "blue",
            strokeWidth: "150",
            transition: "stroke-dashoffset .5s linear",
            strokeDasharray: "500 500",
            strokeDashoffset: "500"
        };
        return (React.createElement("div", { style: wrapper },
            React.createElement("h1", { style: title },
                " ",
                this.text[this.props.id],
                " "),
            React.createElement("div", { style: leftPanel },
                React.createElement("svg", { style: svg },
                    React.createElement("circle", { id: "circleLeft", ref: function (c) { return _this.leftChart = c; }, style: circle, r: "75px", cx: "75px", cy: "75px" })),
                React.createElement("button", { style: btn, onTouchStart: function () { return _this.props.setP(_this.props.id, "left"); } }, " LEFT "),
                React.createElement("h2", { style: label }, " Lab Director ")),
            React.createElement("div", { style: rightPanel },
                React.createElement("svg", { style: svg },
                    React.createElement("circle", { id: "circleRight", ref: function (c) { return _this.rightChart = c; }, style: circle, r: "75", cx: "75", cy: "75" })),
                React.createElement("button", { style: btn, onTouchStart: function () { return _this.props.setP(_this.props.id, "right"); } }, " RIGHT "),
                React.createElement("h2", { style: label }, " Hospital Manager "))));
    };
    return Page;
}(Component));
ReactDOM.render(React.createElement(App, null), document.getElementById('app'));


/***/ })
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgYTI5Nzg4OWExMzA4N2EzMzMyMGQiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiUmVhY3RcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJSZWFjdERPTVwiIiwid2VicGFjazovLy8uL2Rldi93ZWIudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7OztBQzdEQSx1Qjs7Ozs7O0FDQUEsMEI7Ozs7Ozs7Ozs7O0FDQUE7QUFDQTtBQUNBO0FBQ0EsVUFBVSxnQkFBZ0Isc0NBQXNDLGlCQUFpQixFQUFFO0FBQ25GLHlCQUF5Qix1REFBdUQ7QUFDaEY7QUFDQTtBQUNBLHVCQUF1QixzQkFBc0I7QUFDN0M7QUFDQTtBQUNBLENBQUM7QUFDRCw4Q0FBOEMsY0FBYztBQUM1RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FBaUMscUJBQXFCO0FBQ3REO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0Q0FBNEMsaUJBQWlCO0FBQzdELHdDQUF3QyxjQUFjO0FBQ3RELHVDQUF1QyxzREFBc0Q7QUFDN0YsdUNBQXVDLHNEQUFzRDtBQUM3Rix1Q0FBdUMsc0RBQXNEO0FBQzdGLHdDQUF3QyxjQUFjO0FBQ3RELDZDQUE2QyxTQUFTLDRDQUE0QyxFQUFFO0FBQ3BHLG1EQUFtRCwwQ0FBMEMsd0JBQXdCLFVBQVUsRUFBRSxFQUFFLEVBQUU7QUFDckksbURBQW1ELDJDQUEyQyx3QkFBd0IsVUFBVSxFQUFFLEVBQUUsRUFBRTtBQUN0SSxtREFBbUQsMkNBQTJDLHdCQUF3QixVQUFVLEVBQUUsRUFBRSxFQUFFO0FBQ3RJLHdDQUF3QyxlQUFlO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0Q0FBNEMsaUJBQWlCO0FBQzdELHVDQUF1QyxlQUFlO0FBQ3REO0FBQ0E7QUFDQTtBQUNBLHdDQUF3QyxtQkFBbUI7QUFDM0QsNENBQTRDLGFBQWE7QUFDekQsbURBQW1ELHNDQUFzQyw0QkFBNEIsRUFBRSxvREFBb0Q7QUFDM0ssK0NBQStDLHdDQUF3QyxpREFBaUQsRUFBRSxFQUFFO0FBQzVJLDJDQUEyQyxlQUFlO0FBQzFELHdDQUF3QyxvQkFBb0I7QUFDNUQsNENBQTRDLGFBQWE7QUFDekQsbURBQW1ELHVDQUF1Qyw2QkFBNkIsRUFBRSw4Q0FBOEM7QUFDdkssK0NBQStDLHdDQUF3QyxrREFBa0QsRUFBRSxFQUFFO0FBQzdJLDJDQUEyQyxlQUFlO0FBQzFEO0FBQ0E7QUFDQSxDQUFDO0FBQ0QiLCJmaWxlIjoiLi9wdWJsaWMvYXBwLmJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDYpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIGEyOTc4ODlhMTMwODdhMzMzMjBkIiwibW9kdWxlLmV4cG9ydHMgPSBSZWFjdDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyBleHRlcm5hbCBcIlJlYWN0XCJcbi8vIG1vZHVsZSBpZCA9IDBcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiIsIm1vZHVsZS5leHBvcnRzID0gUmVhY3RET007XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gZXh0ZXJuYWwgXCJSZWFjdERPTVwiXG4vLyBtb2R1bGUgaWQgPSAxXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIiLCJcInVzZSBzdHJpY3RcIjtcclxudmFyIF9fZXh0ZW5kcyA9ICh0aGlzICYmIHRoaXMuX19leHRlbmRzKSB8fCAoZnVuY3Rpb24gKCkge1xyXG4gICAgdmFyIGV4dGVuZFN0YXRpY3MgPSBPYmplY3Quc2V0UHJvdG90eXBlT2YgfHxcclxuICAgICAgICAoeyBfX3Byb3RvX186IFtdIH0gaW5zdGFuY2VvZiBBcnJheSAmJiBmdW5jdGlvbiAoZCwgYikgeyBkLl9fcHJvdG9fXyA9IGI7IH0pIHx8XHJcbiAgICAgICAgZnVuY3Rpb24gKGQsIGIpIHsgZm9yICh2YXIgcCBpbiBiKSBpZiAoYi5oYXNPd25Qcm9wZXJ0eShwKSkgZFtwXSA9IGJbcF07IH07XHJcbiAgICByZXR1cm4gZnVuY3Rpb24gKGQsIGIpIHtcclxuICAgICAgICBleHRlbmRTdGF0aWNzKGQsIGIpO1xyXG4gICAgICAgIGZ1bmN0aW9uIF9fKCkgeyB0aGlzLmNvbnN0cnVjdG9yID0gZDsgfVxyXG4gICAgICAgIGQucHJvdG90eXBlID0gYiA9PT0gbnVsbCA/IE9iamVjdC5jcmVhdGUoYikgOiAoX18ucHJvdG90eXBlID0gYi5wcm90b3R5cGUsIG5ldyBfXygpKTtcclxuICAgIH07XHJcbn0pKCk7XHJcbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwgeyB2YWx1ZTogdHJ1ZSB9KTtcclxudmFyIFJlYWN0ID0gcmVxdWlyZShcInJlYWN0XCIpO1xyXG52YXIgUmVhY3RET00gPSByZXF1aXJlKFwicmVhY3QtZG9tXCIpO1xyXG52YXIgQ29tcG9uZW50ID0gUmVhY3QuQ29tcG9uZW50O1xyXG52YXIgc29ja2V0ID0gaW8uY29ubmVjdChcImh0dHA6Ly9cIiArIGxvY2F0aW9uLmhvc3QpO1xyXG52YXIgQXBwID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKF9zdXBlcikge1xyXG4gICAgX19leHRlbmRzKEFwcCwgX3N1cGVyKTtcclxuICAgIGZ1bmN0aW9uIEFwcChwcm9wcykge1xyXG4gICAgICAgIHZhciBfdGhpcyA9IF9zdXBlci5jYWxsKHRoaXMsIHByb3BzKSB8fCB0aGlzO1xyXG4gICAgICAgIF90aGlzLmluaXRUaW1lciA9IGZ1bmN0aW9uIChldikge1xyXG4gICAgICAgICAgICBpZiAoX3RoaXMudGltZXIpIHtcclxuICAgICAgICAgICAgICAgIGNsZWFySW50ZXJ2YWwoX3RoaXMudGltZXIpO1xyXG4gICAgICAgICAgICAgICAgX3RoaXMudGltZXIgPSBudWxsO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIF90aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIHRpbWVyT246IHRydWUsXHJcbiAgICAgICAgICAgICAgICB0aW1lcjogZXZcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIF90aGlzLnRpbWVyID0gc2V0SW50ZXJ2YWwoZnVuY3Rpb24gKGV2KSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgbGFzdCA9IF90aGlzLnN0YXRlLnRpbWVyO1xyXG4gICAgICAgICAgICAgICAgX3RoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIHRpbWVyOiAtLWxhc3RcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgaWYgKGxhc3QgPD0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNsZWFySW50ZXJ2YWwoX3RoaXMudGltZXIpO1xyXG4gICAgICAgICAgICAgICAgICAgIF90aGlzLnRpbWVyID0gbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICBfdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpbWVyOiAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aW1lck9uOiBmYWxzZVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LCAxMDAwKTtcclxuICAgICAgICB9O1xyXG4gICAgICAgIF90aGlzLnNldFBhZ2UgPSBmdW5jdGlvbiAobmlkLCBzaWRlKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwidmlkXCIsIG5pZCwgc2lkZSk7XHJcbiAgICAgICAgICAgIHZhciBpZCA9IG5pZCArIDE7XHJcbiAgICAgICAgICAgIHNvY2tldC5lbWl0KFwicGxheVwiLCB7IGlkOiBpZCwgc2lkZTogc2lkZSB9KTtcclxuICAgICAgICB9O1xyXG4gICAgICAgIF90aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICBwYWdlOiAwLFxyXG4gICAgICAgICAgICB0aW1lcjogMCxcclxuICAgICAgICAgICAgdGltZXJPbjogZmFsc2VcclxuICAgICAgICB9O1xyXG4gICAgICAgIF90aGlzLnRpbWVyID0gbnVsbDtcclxuICAgICAgICByZXR1cm4gX3RoaXM7XHJcbiAgICB9XHJcbiAgICBBcHAucHJvdG90eXBlLmNvbXBvbmVudERpZE1vdW50ID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJtdGRzXCIpO1xyXG4gICAgICAgIHNvY2tldC5vbihcIndhaXRcIiwgZnVuY3Rpb24gKHRpbWUpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJ3YWl0XCIsIHRpbWUsIFwic2Vjb25kc1wiKTtcclxuICAgICAgICAgICAgX3RoaXMuaW5pdFRpbWVyKHRpbWUudG9GaXhlZCgwKSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG4gICAgQXBwLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcclxuICAgICAgICB2YXIgd3JhcHBlciA9IHtcclxuICAgICAgICAgICAgbWFyZ2luVG9wOiBcIjEzMHB4XCIsXHJcbiAgICAgICAgICAgIHdpZHRoOiBcIjEwMjRweFwiLFxyXG4gICAgICAgICAgICBoZWlnaHQ6IFwiNzY4cHhcIixcclxuICAgICAgICAgICAgcG9zaXRpb246IFwiYWJzb2x1dGVcIixcclxuICAgICAgICAgICAgdG9wOiAwLFxyXG4gICAgICAgICAgICBsZWZ0OiAwLFxyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiBcIi13ZWJraXQtbGluZWFyLWdyYWRpZW50KGxlZnQgdG9wLHdoaXRlICxibHVlIDkwJSwgYmx1ZSlcIlxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdmFyIHRhYnMgPSB7XHJcbiAgICAgICAgICAgIHdpZHRoOiBcIjEwMjRweFwiLFxyXG4gICAgICAgICAgICBoZWlnaHQ6IFwiMTAwcHhcIixcclxuICAgICAgICAgICAgcG9zaXRpb246IFwiYWJzb2x1dGVcIixcclxuICAgICAgICAgICAgYm90dG9tOiAwLFxyXG4gICAgICAgICAgICBsZWZ0OiAwLFxyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiBcInRyYW5zcGFyZW50XCJcclxuICAgICAgICB9O1xyXG4gICAgICAgIHZhciBidXR0b24gPSB7XHJcbiAgICAgICAgICAgIHdpZHRoOiBcIjgwcHhcIixcclxuICAgICAgICAgICAgaGVpZ2h0OiBcIjgwcHhcIixcclxuICAgICAgICAgICAgYm9yZGVyUmFkaXVzOiBcIjQwcHhcIixcclxuICAgICAgICAgICAgYm9yZGVyOiBcIm5vbmVcIixcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogdGhpcy5zdGF0ZS5wYWdlID09PSAwID8gXCJ3aGl0ZVwiIDogXCJyZ2JhKDAsMCwyNTUsLjYpXCIsXHJcbiAgICAgICAgICAgIG9wYWNpdHk6IHRoaXMuc3RhdGUucGFnZSA9PT0gMCA/IFwiMVwiIDogXCIuNVwiLFxyXG4gICAgICAgICAgICB0ZXh0RGVjb3JhdGlvbjogdGhpcy5zdGF0ZS5wYWdlID09PSAwID8gXCJ1bmRlcmxpbmVcIiA6IFwibm9uZVwiLFxyXG4gICAgICAgICAgICBtYXJnaW46IFwiMTBweCAxMHB4XCIsXHJcbiAgICAgICAgICAgIHRyYW5zaXRpb246IFwiYWxsIC41cyBsaW5lYXJcIixcclxuICAgICAgICAgICAgZm9udFNpemU6IFwiMjBweFwiLFxyXG4gICAgICAgICAgICBXZWJraXRGaWx0ZXI6IFwiZHJvcC1zaGFkb3coMnB4IDJweCA0cHggcmdiYSgwLDAsMCwuMikpXCJcclxuICAgICAgICB9O1xyXG4gICAgICAgIHZhciBidXR0b24yID0ge1xyXG4gICAgICAgICAgICB3aWR0aDogXCI4MHB4XCIsXHJcbiAgICAgICAgICAgIGhlaWdodDogXCI4MHB4XCIsXHJcbiAgICAgICAgICAgIGJvcmRlclJhZGl1czogXCI0MHB4XCIsXHJcbiAgICAgICAgICAgIGJvcmRlcjogXCJub25lXCIsXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6IHRoaXMuc3RhdGUucGFnZSA9PT0gMSA/IFwid2hpdGVcIiA6IFwicmdiYSgwLDAsMjU1LC42KVwiLFxyXG4gICAgICAgICAgICB0ZXh0RGVjb3JhdGlvbjogdGhpcy5zdGF0ZS5wYWdlID09PSAxID8gXCJ1bmRlcmxpbmVcIiA6IFwibm9uZVwiLFxyXG4gICAgICAgICAgICBtYXJnaW46IFwiMTBweCAxMHB4XCIsXHJcbiAgICAgICAgICAgIG9wYWNpdHk6IHRoaXMuc3RhdGUucGFnZSA9PT0gMSA/IFwiMVwiIDogXCIuNVwiLFxyXG4gICAgICAgICAgICB0cmFuc2l0aW9uOiBcImFsbCAuNXMgbGluZWFyXCIsXHJcbiAgICAgICAgICAgIGZvbnRTaXplOiBcIjIwcHhcIixcclxuICAgICAgICAgICAgV2Via2l0RmlsdGVyOiBcImRyb3Atc2hhZG93KDJweCAycHggNHB4IHJnYmEoMCwwLDAsLjIpKVwiXHJcbiAgICAgICAgfTtcclxuICAgICAgICB2YXIgYnV0dG9uMyA9IHtcclxuICAgICAgICAgICAgd2lkdGg6IFwiODBweFwiLFxyXG4gICAgICAgICAgICBoZWlnaHQ6IFwiODBweFwiLFxyXG4gICAgICAgICAgICBib3JkZXJSYWRpdXM6IFwiNDBweFwiLFxyXG4gICAgICAgICAgICBib3JkZXI6IFwibm9uZVwiLFxyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiB0aGlzLnN0YXRlLnBhZ2UgPT09IDIgPyBcIndoaXRlXCIgOiBcInJnYmEoMCwwLDI1NSwuNilcIixcclxuICAgICAgICAgICAgdGV4dERlY29yYXRpb246IHRoaXMuc3RhdGUucGFnZSA9PT0gMiA/IFwidW5kZXJsaW5lXCIgOiBcIm5vbmVcIixcclxuICAgICAgICAgICAgb3BhY2l0eTogdGhpcy5zdGF0ZS5wYWdlID09PSAyID8gXCIxXCIgOiBcIi41XCIsXHJcbiAgICAgICAgICAgIG1hcmdpbjogXCIxMHB4IDEwcHhcIixcclxuICAgICAgICAgICAgdHJhbnNpdGlvbjogXCJhbGwgLjVzIGxpbmVhclwiLFxyXG4gICAgICAgICAgICBmb250U2l6ZTogXCIyMHB4XCIsXHJcbiAgICAgICAgICAgIFdlYmtpdEZpbHRlcjogXCJkcm9wLXNoYWRvdygycHggMnB4IDRweCByZ2JhKDAsMCwwLC4yKSlcIlxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdmFyIHRpbWVyID0ge1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogXCJhYnNvbHV0ZVwiLFxyXG4gICAgICAgICAgICB3aWR0aDogXCI1MHB4XCIsXHJcbiAgICAgICAgICAgIGhlaWdodDogXCI1MHB4XCIsXHJcbiAgICAgICAgICAgIHRvcDogXCJjYWxjKDUwJSAtIDI1cHgpXCIsXHJcbiAgICAgICAgICAgIGxlZnQ6IFwiY2FsYyg1MCUgLSAyNXB4KVwiLFxyXG4gICAgICAgICAgICBwaW50ZXJFdmVudHM6IFwibm9uZVwiLFxyXG4gICAgICAgICAgICBvcGFjaXR5OiB0aGlzLnN0YXRlLnRpbWVyT24gPyBcIjFcIiA6IFwiMFwiXHJcbiAgICAgICAgfTtcclxuICAgICAgICB2YXIgZGVzYyA9IHtcclxuICAgICAgICAgICAgY29sb3I6IFwid2hpdGVcIixcclxuICAgICAgICAgICAgcG9zaXRpb246IFwiYWJzb2x1dGVcIixcclxuICAgICAgICAgICAgcmlnaHQ6IFwiNTBweFwiLFxyXG4gICAgICAgICAgICB0b3A6IFwiNTBweFwiLFxyXG4gICAgICAgICAgICBmb250U2l6ZTogXCIyMnB4XCJcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiAoUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7IHN0eWxlOiB3cmFwcGVyIH0sXHJcbiAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwgeyBzdHlsZTogZGVzYyB9LCBcIiBUb2NjYSBzdWwgcHVsc2FudGUgcGVyIHNlbnRpcmUgbGEgcmlzcG9zdGEgLi4uICAgXCIpLFxyXG4gICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFBhZ2UsIHsgc2V0UDogdGhpcy5zZXRQYWdlLCBjdXJyZW50OiB0aGlzLnN0YXRlLnBhZ2UsIGlkOiAwIH0pLFxyXG4gICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFBhZ2UsIHsgc2V0UDogdGhpcy5zZXRQYWdlLCBjdXJyZW50OiB0aGlzLnN0YXRlLnBhZ2UsIGlkOiAxIH0pLFxyXG4gICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFBhZ2UsIHsgc2V0UDogdGhpcy5zZXRQYWdlLCBjdXJyZW50OiB0aGlzLnN0YXRlLnBhZ2UsIGlkOiAyIH0pLFxyXG4gICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHsgc3R5bGU6IHRhYnMgfSxcclxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzcGFuXCIsIHsgc3R5bGU6IHsgdGV4dEFsaWduOiBcImNlbnRlclwiLCBwYWRkaW5nTGVmdDogXCIzNTBweFwiIH0gfSxcclxuICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiYnV0dG9uXCIsIHsgc3R5bGU6IGJ1dHRvbiwgb25Ub3VjaEVuZDogZnVuY3Rpb24gKGUpIHsgcmV0dXJuIF90aGlzLnNldFN0YXRlKHsgcGFnZTogMCB9KTsgfSB9LCBcIiAxIFwiKSxcclxuICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiYnV0dG9uXCIsIHsgc3R5bGU6IGJ1dHRvbjIsIG9uVG91Y2hFbmQ6IGZ1bmN0aW9uIChlKSB7IHJldHVybiBfdGhpcy5zZXRTdGF0ZSh7IHBhZ2U6IDEgfSk7IH0gfSwgXCIgMiBcIiksXHJcbiAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImJ1dHRvblwiLCB7IHN0eWxlOiBidXR0b24zLCBvblRvdWNoRW5kOiBmdW5jdGlvbiAoZSkgeyByZXR1cm4gX3RoaXMuc2V0U3RhdGUoeyBwYWdlOiAyIH0pOyB9IH0sIFwiIDMgXCIpKSksXHJcbiAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwgeyBzdHlsZTogdGltZXIgfSxcclxuICAgICAgICAgICAgICAgIFwiIFwiLFxyXG4gICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS50aW1lcixcclxuICAgICAgICAgICAgICAgIFwiIFwiKSkpO1xyXG4gICAgfTtcclxuICAgIHJldHVybiBBcHA7XHJcbn0oQ29tcG9uZW50KSk7XHJcbnZhciBQYWdlID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKF9zdXBlcikge1xyXG4gICAgX19leHRlbmRzKFBhZ2UsIF9zdXBlcik7XHJcbiAgICBmdW5jdGlvbiBQYWdlKHByb3BzKSB7XHJcbiAgICAgICAgdmFyIF90aGlzID0gX3N1cGVyLmNhbGwodGhpcywgcHJvcHMpIHx8IHRoaXM7XHJcbiAgICAgICAgX3RoaXMudGV4dCA9IFtcclxuICAgICAgICAgICAgXCJRdWFsaSBzb25vIGxlIHNmaWRlIHByaW5jaXBhbGkgY2hlIGxlaSBkZXZlIGFmZnJvbnRhcmUgbmVsIGJyZXZlIGUgbmVsIGx1bmdvIHRlcm1pbmU/XCIsXHJcbiAgICAgICAgICAgIFwiQ29zYSBwb3RyZWJiZSBhaXV0YXJsYSBuZWxsJ2FmZnJvbnRhcmUgcXVlc3RlIHNmaWRlP1wiLFxyXG4gICAgICAgICAgICBcIkNvc2EgaGEgdmlzdG8gcmVjZW50ZW1lbnRlP1wiXHJcbiAgICAgICAgXTtcclxuICAgICAgICByZXR1cm4gX3RoaXM7XHJcbiAgICB9XHJcbiAgICBQYWdlLnByb3RvdHlwZS5jb21wb25lbnREaWRNb3VudCA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG4gICAgICAgIHNvY2tldC5vbihcInVwZGF0ZVwiLCBmdW5jdGlvbiAoZGF0YSkge1xyXG4gICAgICAgICAgICBpZiAoX3RoaXMucHJvcHMuaWQgKyAxID09PSBkYXRhLmlkKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgbnIgPSA1MDAgLSAoTnVtYmVyKGRhdGEudGltZSkgKiA1KTtcclxuICAgICAgICAgICAgICAgIGlmIChuciA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIF90aGlzLmxlZnRDaGFydC5zdHlsZS5zdHJva2VEYXNob2Zmc2V0ID0gXCI1MDBcIjtcclxuICAgICAgICAgICAgICAgICAgICBfdGhpcy5yaWdodENoYXJ0LnN0eWxlLnN0cm9rZURhc2hvZmZzZXQgPSBcIjUwMFwiO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmIChkYXRhLnNpZGUgPT09IFwibGVmdFwiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgX3RoaXMubGVmdENoYXJ0LnN0eWxlLnN0cm9rZURhc2hvZmZzZXQgPSBcIlwiICsgbnI7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBfdGhpcy5yaWdodENoYXJ0LnN0eWxlLnN0cm9rZURhc2hvZmZzZXQgPSBcIlwiICsgbnI7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBfdGhpcy5sZWZ0Q2hhcnQuc3R5bGUuc3Ryb2tlRGFzaG9mZnNldCA9IFwiNTAwXCI7XHJcbiAgICAgICAgICAgICAgICBfdGhpcy5yaWdodENoYXJ0LnN0eWxlLnN0cm9rZURhc2hvZmZzZXQgPSBcIjUwMFwiO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG4gICAgUGFnZS5wcm90b3R5cGUuY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcyA9IGZ1bmN0aW9uIChucCkge1xyXG4gICAgICAgIC8vICBjb25zb2xlLmxvZyhcIm5ld1wiLCBucCk7XHJcbiAgICB9O1xyXG4gICAgUGFnZS5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcbiAgICAgICAgdmFyIHdyYXBwZXIgPSB7XHJcbiAgICAgICAgICAgIHdpZHRoOiBcIjEwMCVcIixcclxuICAgICAgICAgICAgaGVpZ2h0OiBcImNhbGMoMTAwJSAtIDEwMHB4KVwiLFxyXG4gICAgICAgICAgICBwb3NpdGlvbjogXCJhYnNvbHV0ZVwiLFxyXG4gICAgICAgICAgICB0b3A6IDAsXHJcbiAgICAgICAgICAgIGxlZnQ6IDAsXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6IFwidHJhbnNwYXJlbnRcIixcclxuICAgICAgICAgICAgdHJhbnNpdGlvbjogXCJhbGwgLjZzIGVhc2Utb3V0XCIsXHJcbiAgICAgICAgICAgIFdlYmtpdFRyYW5zZm9ybTogdGhpcy5wcm9wcy5jdXJyZW50ID09PSB0aGlzLnByb3BzLmlkID8gXCJzY2FsZSgxKVwiIDogXCJzY2FsZSgwKVwiLFxyXG4gICAgICAgICAgICBvcGFjaXR5OiB0aGlzLnByb3BzLmN1cnJlbnQgPT09IHRoaXMucHJvcHMuaWQgPyBcIjFcIiA6IFwiMFwiLFxyXG4gICAgICAgICAgICBwb2ludGVyRXZlbnRzOiB0aGlzLnByb3BzLmN1cnJlbnQgPT09IHRoaXMucHJvcHMuaWQgPyBcImF1dG9cIiA6IFwibm9uZVwiXHJcbiAgICAgICAgfTtcclxuICAgICAgICB2YXIgdGl0bGUgPSB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBcImFic29sdXRlXCIsXHJcbiAgICAgICAgICAgIGJveFNpemluZzogXCJib3JkZXItYm94XCIsXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6IFwid2hpdGVcIixcclxuICAgICAgICAgICAgcGFkZGluZzogXCIyMHB4IDUwcHhcIixcclxuICAgICAgICAgICAgd2lkdGg6IFwiMTAwJVwiLFxyXG4gICAgICAgICAgICB0b3A6IFwiMTAwcHhcIixcclxuICAgICAgICAgICAgYm94U2hhZG93OiBcIjBweCA1cHggMTBweCAtNXB4IGJsYWNrXCIsXHJcbiAgICAgICAgICAgIHRleHRBbGlnbjogXCJjZW50ZXJcIlxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdmFyIGxlZnRQYW5lbCA9IHtcclxuICAgICAgICAgICAgcG9zaXRpb246IFwiYWJzb2x1dGVcIixcclxuICAgICAgICAgICAgd2lkdGg6IFwiMzAwcHhcIixcclxuICAgICAgICAgICAgaGVpZ2h0OiBcIjMwMHB4XCIsXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6IFwidHJhbnNwYXJlbnRcIixcclxuICAgICAgICAgICAgbGVmdDogXCIxMDBweFwiLFxyXG4gICAgICAgICAgICBib3R0b206IFwiMTAwcHhcIlxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdmFyIHJpZ2h0UGFuZWwgPSB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBcImFic29sdXRlXCIsXHJcbiAgICAgICAgICAgIHdpZHRoOiBcIjMwMHB4XCIsXHJcbiAgICAgICAgICAgIGhlaWdodDogXCIzMDBweFwiLFxyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiBcInRyYW5zcGFyZW50XCIsXHJcbiAgICAgICAgICAgIHJpZ2h0OiBcIjEwMHB4XCIsXHJcbiAgICAgICAgICAgIGJvdHRvbTogXCIxMDBweFwiXHJcbiAgICAgICAgfTtcclxuICAgICAgICB2YXIgbGFiZWwgPSB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBcImFic29sdXRlXCIsXHJcbiAgICAgICAgICAgIHdpZHRoOiBcIjEwMCVcIixcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogXCJyZ2JhKDAsMCwyNTUsMClcIixcclxuICAgICAgICAgICAgY29sb3I6IFwid2hpdGVcIixcclxuICAgICAgICAgICAgYm90dG9tOiAwLFxyXG4gICAgICAgICAgICBsZWZ0OiAwLFxyXG4gICAgICAgICAgICBtYXJnaW46IDAsXHJcbiAgICAgICAgICAgIHRleHRBbGlnbjogXCJjZW50ZXJcIlxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdmFyIGJ0biA9IHtcclxuICAgICAgICAgICAgcG9zaXRpb246IFwiYWJzb2x1dGVcIixcclxuICAgICAgICAgICAgd2lkdGg6IFwiMTUwcHhcIixcclxuICAgICAgICAgICAgaGVpZ2h0OiBcIjE1MHB4XCIsXHJcbiAgICAgICAgICAgIHRvcDogXCI3NXB4XCIsXHJcbiAgICAgICAgICAgIGxlZnQ6IFwiNzVweFwiLFxyXG4gICAgICAgICAgICBib3JkZXI6IFwibm9uZVwiLFxyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiBcInRyYW5zcGFyZW50XCIsXHJcbiAgICAgICAgICAgIGJvcmRlclJhZGl1czogXCI3NXB4XCJcclxuICAgICAgICB9O1xyXG4gICAgICAgIHZhciBzdmcgPSB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBcImFic29sdXRlXCIsXHJcbiAgICAgICAgICAgIHdpZHRoOiBcIjE1MHB4XCIsXHJcbiAgICAgICAgICAgIGhlaWdodDogXCIxNTBweFwiLFxyXG4gICAgICAgICAgICB0b3A6IFwiNzVweFwiLFxyXG4gICAgICAgICAgICBsZWZ0OiBcIjc1cHhcIixcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogXCJ3aGl0ZVwiLFxyXG4gICAgICAgICAgICBXZWJraXRUcmFuc2Zvcm06IFwicm90YXRlKC05MGRlZylcIixcclxuICAgICAgICAgICAgYm9yZGVyUmFkaXVzOiBcIjc1cHhcIixcclxuICAgICAgICAgICAgcG9pbnRlckV2ZW50czogXCJub25lXCIsXHJcbiAgICAgICAgICAgIFdlYmtpdEZpbHRlcjogXCJkcm9wLXNoYWRvdygtNXB4IDJweCA1cHggcmdiYSgwLDAsMCwuNSkpXCJcclxuICAgICAgICB9O1xyXG4gICAgICAgIHZhciBjaXJjbGUgPSB7XHJcbiAgICAgICAgICAgIGZpbGw6IFwid2hpdGVcIixcclxuICAgICAgICAgICAgc3Ryb2tlOiBcImJsdWVcIixcclxuICAgICAgICAgICAgc3Ryb2tlV2lkdGg6IFwiMTUwXCIsXHJcbiAgICAgICAgICAgIHRyYW5zaXRpb246IFwic3Ryb2tlLWRhc2hvZmZzZXQgLjVzIGxpbmVhclwiLFxyXG4gICAgICAgICAgICBzdHJva2VEYXNoYXJyYXk6IFwiNTAwIDUwMFwiLFxyXG4gICAgICAgICAgICBzdHJva2VEYXNob2Zmc2V0OiBcIjUwMFwiXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gKFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwgeyBzdHlsZTogd3JhcHBlciB9LFxyXG4gICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaDFcIiwgeyBzdHlsZTogdGl0bGUgfSxcclxuICAgICAgICAgICAgICAgIFwiIFwiLFxyXG4gICAgICAgICAgICAgICAgdGhpcy50ZXh0W3RoaXMucHJvcHMuaWRdLFxyXG4gICAgICAgICAgICAgICAgXCIgXCIpLFxyXG4gICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHsgc3R5bGU6IGxlZnRQYW5lbCB9LFxyXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcInN2Z1wiLCB7IHN0eWxlOiBzdmcgfSxcclxuICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiY2lyY2xlXCIsIHsgaWQ6IFwiY2lyY2xlTGVmdFwiLCByZWY6IGZ1bmN0aW9uIChjKSB7IHJldHVybiBfdGhpcy5sZWZ0Q2hhcnQgPSBjOyB9LCBzdHlsZTogY2lyY2xlLCByOiBcIjc1cHhcIiwgY3g6IFwiNzVweFwiLCBjeTogXCI3NXB4XCIgfSkpLFxyXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImJ1dHRvblwiLCB7IHN0eWxlOiBidG4sIG9uVG91Y2hTdGFydDogZnVuY3Rpb24gKCkgeyByZXR1cm4gX3RoaXMucHJvcHMuc2V0UChfdGhpcy5wcm9wcy5pZCwgXCJsZWZ0XCIpOyB9IH0sIFwiIExFRlQgXCIpLFxyXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImgyXCIsIHsgc3R5bGU6IGxhYmVsIH0sIFwiIExhYiBEaXJlY3RvciBcIikpLFxyXG4gICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHsgc3R5bGU6IHJpZ2h0UGFuZWwgfSxcclxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzdmdcIiwgeyBzdHlsZTogc3ZnIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImNpcmNsZVwiLCB7IGlkOiBcImNpcmNsZVJpZ2h0XCIsIHJlZjogZnVuY3Rpb24gKGMpIHsgcmV0dXJuIF90aGlzLnJpZ2h0Q2hhcnQgPSBjOyB9LCBzdHlsZTogY2lyY2xlLCByOiBcIjc1XCIsIGN4OiBcIjc1XCIsIGN5OiBcIjc1XCIgfSkpLFxyXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImJ1dHRvblwiLCB7IHN0eWxlOiBidG4sIG9uVG91Y2hTdGFydDogZnVuY3Rpb24gKCkgeyByZXR1cm4gX3RoaXMucHJvcHMuc2V0UChfdGhpcy5wcm9wcy5pZCwgXCJyaWdodFwiKTsgfSB9LCBcIiBSSUdIVCBcIiksXHJcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaDJcIiwgeyBzdHlsZTogbGFiZWwgfSwgXCIgSG9zcGl0YWwgTWFuYWdlciBcIikpKSk7XHJcbiAgICB9O1xyXG4gICAgcmV0dXJuIFBhZ2U7XHJcbn0oQ29tcG9uZW50KSk7XHJcblJlYWN0RE9NLnJlbmRlcihSZWFjdC5jcmVhdGVFbGVtZW50KEFwcCwgbnVsbCksIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdhcHAnKSk7XHJcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vZGV2L3dlYi50c3hcbi8vIG1vZHVsZSBpZCA9IDZcbi8vIG1vZHVsZSBjaHVua3MgPSAyIl0sInNvdXJjZVJvb3QiOiIifQ==